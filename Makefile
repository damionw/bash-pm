PACKAGE_NAME := bashpm

INSTALL_PATH := $(HOME)/.local
PACKAGE_VERSION := $(shell bash -c '. src/lib/$(PACKAGE_NAME) 2>/dev/null; $(PACKAGE_NAME)::version')
LIB_COMPONENTS := $(wildcard src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/*)
BIN_COMPONENTS := $(foreach name, $(wildcard src/bin/*), build/bin/$(notdir $(name)))
DIR_COMPONENTS := $(foreach name, bin lib, build/$(name)) packages tools
PREREQUISITES := jq

#=======================================================================
#
#=======================================================================
.PHONY: tests clean realclean help

#=======================================================================
#
#=======================================================================
all: build

help:
	@echo "Usage: make build|tests|all|run|clean|version|install|install-private"

version: all
	@build/bin/$(PACKAGE_NAME) --version

install: tests
	@echo "Installing into directory '$(INSTALL_PATH)'"
	@rsync -az build/ $(INSTALL_PATH)/

build: build/lib/$(PACKAGE_NAME) \
	$(BIN_COMPONENTS) \
	$(LIB_COMPONENTS)

tests: all
	@PATH="$(shell readlink -f build/bin):$(PATH)" tests/testsuite

#=======================================================================
#
#=======================================================================
build/lib/$(PACKAGE_NAME): build/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION) build/lib src/lib/$(PACKAGE_NAME) \
	src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/option_parsing \
	src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/logging
	@install -m 755 src/lib/$(PACKAGE_NAME) $@

build/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION): build/lib $(LIB_COMPONENTS)
	@rsync -az src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/ $@/

build/bin/%: build/lib/$(PACKAGE_NAME) build/bin | src/bin
	@install -m 755 src/bin/$(notdir $@) $@

#=======================================================================
#
#=======================================================================
src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/option_parsing: checkouts/optionslib .FORCE
	@cd $< && make all
	@cp $</build/lib/optionslib-$$($</build/bin/optionslib --version)/parse $@

src/lib/$(PACKAGE_NAME)-$(PACKAGE_VERSION)/logging: checkouts/bashlib .FORCE
	@cd $< && make all
	@cp $</build/lib/bashLib-$$($</build/bin/bashlib --version)/$(notdir $@) $@

#=======================================================================
#
#=======================================================================
checkouts/optionslib: checkouts
	@(cd "$@" >/dev/null 2>&1 && git pull) || git clone https://github.com/damionw/optionslib.git $@

checkouts/bashlib: checkouts
	@(cd "$@" >/dev/null 2>&1 && git pull) || git clone https://github.com/damionw/bashlib.git $@

#=======================================================================
#
#=======================================================================
$(DIR_COMPONENTS) checkouts:
	@install -d $@

#=======================================================================
#
#=======================================================================
clean:
	-@rm -rf build packages tools data

realclean: clean
	-@rm -rf checkouts

.FORCE:
