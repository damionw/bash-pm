#!/usr/bin/env bash

#===================================================================================
#                               Select Package
#===================================================================================
package_name=bashpm
default_registry="https://codeberg.org/damionw/bash-registry"

#===================================================================================
#                              Determine Paths
#===================================================================================
first_name="${BASH_SOURCE[0]}"
actual_name="$(readlink -f "${first_name}")"
local_path="$(dirname "${actual_name}")"

#===================================================================================
#                                Settings
#===================================================================================
export __BASH_PACKAGE_MANAGER_VERSION__="$(
    find "${local_path}/${package_name}"-[.0-9]* -maxdepth 0 -mindepth 0 -type d -printf "%f\n" |
    awk -F- '{print $NF;}' |
    sort -nr |
    head -1
)"

lib_path="${local_path}/${package_name}-${__BASH_PACKAGE_MANAGER_VERSION__}"

export __BASH_PACKAGE_MANAGER_SUBCOMMANDS__="${lib_path}/subcommands"
export __BASH_PACKAGE_MANAGER_REGISTRY__="${__BASH_PACKAGE_MANAGER_REGISTRY__:-${default_registry}}"
export __BASH_PACKAGE_MANAGER_CACHE__="${__BASH_PACKAGE_MANAGER_CACHE__:-"${HOME}/.${package_name}"}"
export __BASH_PACKAGE_INSTALL_ROOT__="${__BASH_PACKAGE_INSTALL_ROOT__:-"${HOME}/.local"}"

. "${lib_path}/logging"
. "${lib_path}/option_parsing"
. "${lib_path}/packages"
. "${lib_path}/list"
. "${lib_path}/paths"

bashpm::version() {
    echo "${__BASH_PACKAGE_MANAGER_VERSION__}"
}

bashpm::subcommands::path() {
    if [ -n "${__BASH_PACKAGE_MANAGER_SUBCOMMANDS__}" -a -d "${__BASH_PACKAGE_MANAGER_SUBCOMMANDS__}/." ]
    then
        echo "${__BASH_PACKAGE_MANAGER_SUBCOMMANDS__}"
    fi
}

bashpm::subcommands() {
    find "$(bashpm::subcommands::path)/" -mindepth 1 -maxdepth 1 -type f -executable -printf "%f\n" 2>/dev/null | sort
}

bashpm::registry_url() {
    if [ -n "${1}" ]
    then
        export __BASH_PACKAGE_MANAGER_REGISTRY__="${1}"
        return
    fi

    if [ -z "${__BASH_PACKAGE_MANAGER_REGISTRY__}" ]
    then
        logging::error "No package registry has been selected"
        return 11
    fi

    echo "${__BASH_PACKAGE_MANAGER_REGISTRY__}"
}

bashpm::registry_cache() {
    if [ -n "${1}" ]
    then
        export __BASH_PACKAGE_MANAGER_CACHE__="${1}"
        return
    fi

    if [ -z "${__BASH_PACKAGE_MANAGER_CACHE__}" ]
    then
        logging::error "No manifest cache folder has been declared"
        return 19
    fi

    if [ -d "${__BASH_PACKAGE_MANAGER_CACHE__}" ]
    then
        :
    elif mkdir "${__BASH_PACKAGE_MANAGER_CACHE__}"
    then
        logging::warning "Created cache ${__BASH_PACKAGE_MANAGER_CACHE__}"
    else
        logging::error "Can't use cache folder ${__BASH_PACKAGE_MANAGER_CACHE__}"
        return 99
    fi

    echo "${__BASH_PACKAGE_MANAGER_CACHE__}"
}

bashpm::install_root() {
    if [ -n "${1}" ]
    then
        export __BASH_PACKAGE_INSTALL_ROOT__="${1}"
        return
    fi

    if [ -z "${__BASH_PACKAGE_INSTALL_ROOT__}" ]
    then
        logging::error "No manifest cache folder has been declared"
        return 19
    fi

    if [ -d "${__BASH_PACKAGE_INSTALL_ROOT__}" ]
    then
        :
    else
        logging::error "Can't use cache folder ${__BASH_PACKAGE_INSTALL_ROOT__}"
        return 99
    fi

    echo "${__BASH_PACKAGE_INSTALL_ROOT__}"
}

hash -r
