#!/usr/bin/env bash

#===================================================================================
#                               Select Package
#===================================================================================
package_name=bashpm

#===================================================================================
#                              Determine Paths
#===================================================================================
first_name="${BASH_SOURCE[0]}"
actual_name="$(readlink -f "${first_name}")"
local_path="$(dirname "${actual_name}")"
library_import_file="$(readlink -f "${local_path}/../lib/${package_name}")"

#===================================================================================
#                          Import tools library
#===================================================================================
. "${library_import_file}"

#===================================================================================
#                              Logging Options
#===================================================================================
logging::set_severity info

#===================================================================================
#                                Execute a subcommand if required
#===================================================================================
if [ -z "$1" ]
then
    :
elif (echo "$1" | grep -q '^[\-]')
then
    :
else
    subcommand="$(${package_name}::subcommands::path)/${1}"
    shift

    if [ -x "${subcommand}" ]
    then
        PATH="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")"):${PATH}" exec "${subcommand}" "$@"
    else
        logging::fatal "No such command '${subcommand}'"
    fi
fi

#===================================================================================
#                           Command line option handlers
#===================================================================================
set_logging() {
    logging::set_severity "$(echo "${1}" | sed -e 's/^[\-]*//g')"
}

show_library_path() {
    echo "${library_import_file}"
}

#===================================================================================
#                           Process command line options
#===================================================================================
optionslib::parse::description "Bash Package Manager"

optionslib::parse::config "
    long_options=--help short_options=-h action=show_help description='Display instructions'
    long_options=--debug action=command name=set_logging description='Expose debug level logging'
    long_options=--info action=command name=set_logging  description='Expose normal level logging'
    long_options=--warning action=command name=set_logging  description='Expose error level logging'
    long_options=--error action=command name=set_logging  description='Expose error level logging'
    long_options=--fatal action=command name=set_logging  description='Expose fatal error level logging'
    long_options=--lib action=command name=show_library_path description='Provide the library module import file'
    long_options=--version short_options=-v action=command name=${package_name}::version description='Produce the library version string'
    long_options=--commands action=command name=${package_name}::subcommands description='List available subcommands'
    long_options=--registry:: action=command name=${package_name}::registry_url description='Select/Show package registry'
    long_options=--packages action=command name=${package_name}::packages::list description='List available packages'
    long_options=--cache:: action=command name=${package_name}::bashpm::registry_cache description='Select/Show registry cache folder'
"

optionslib::parse::parse_arguments "$@" || {
    exit $?
}
